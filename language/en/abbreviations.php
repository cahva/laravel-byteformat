<?php

return array(
	'terabyte' => "TB",
	'gigabyte' => "GB",
	'megabyte' => "MB",
	'kilobyte' => "KB",
	'bytes' => "Bytes"
);