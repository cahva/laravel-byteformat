<?php

class Byteformat {
	

	public static function get($num, $precision = 1)
	{

		if ($num >= 1000000000000)
		{
			$num = round($num / 1099511627776, $precision);
			$unit = __('Byteformat::abbreviations.terabyte');
		}
		elseif ($num >= 1000000000)
		{
			$num = round($num / 1073741824, $precision);
			$unit = __('Byteformat::abbreviations.gigabyte');
		}
		elseif ($num >= 1000000)
		{
			$num = round($num / 1048576, $precision);
			$unit = __('Byteformat::abbreviations.megabyte');
		}
		elseif ($num >= 1000)
		{
			$num = round($num / 1024, $precision);
			$unit = __('Byteformat::abbreviations.kilobyte');
		}
		else
		{
			$unit = __('Byteformat::abbreviations.bytes');
			return number_format($num).' '.$unit;
		}

		return number_format($num, $precision).' '.$unit;
	}
}