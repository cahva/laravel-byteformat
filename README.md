# Byteformat

Formats a number as bytes, based on size, and adds the appropriate suffix.

Based on [CodeIgniter's][1] [number helper][2] and made it work as a laravel bundle.


## Usage

Add bundle to application/bundles.php. Either set it to autoload or start it with `Bundle::start('byteformat');`

Using it is simple as:

	:::php
		echo Byteformat::get(1000000); // Outputs 1.0 MB


- - -

Can't take credit for this, just modified to use Laravel's localization and made it a static class, nothing more.

[1]: http://codeigniter.com/
[2]: http://codeigniter.com/user_guide/helpers/number_helper.html